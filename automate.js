
var tools = require('./tools');
var server = require('./server');

class AutomateController {
    constructor(config) {
        this.timeout = 1000;
        this.systems = [];
        this.requestHandlers = {};
        this.timeoutHandle = null;

        var serverConfig = Object.assign({
            parent: this
        }, config.server);

        // Clone object, and assign 'parent property'
        this.server = new server.Server(Object.assign({
                parent: this
            }, 
            config.server
        ));

        if(config.timeout != undefined) {
            this.timeout = config.timeout;
        }

        this.update();
    }

    getSystemHandler(key) {
        return this.requestHandlers[key];
    }

    getSystems() {
        return this.systems;
    }

    get(config) {
        switch(config.type) {
            case "all": {
                var data = [];
                for(var key in this.requestHandlers) {
                    data.push(this.requestHandlers[key].toObject());
                }
                return data;
            }
        }
        return null;
    }

    // heartbeat, called every timoeout
    update() {
        this.systems.forEach(function(system) {
            system.update();
        });

        // Recall update after some time
        this.timeoutHandle = setTimeout(this.update.bind(this), this.timeout);
    }

    generateHandle() {
        return Math.random().toString(36).substring(7);
    }

    addSystem(config) {
        config.system.register({handle: this.generateHandle()});
        if(config.requestHandler != null) {
            this.requestHandlers[config.system.getHandle()] = config.requestHandler;
        }
        this.systems.push(config.system);
    }

    removeSystem(sys) {
        for(var i = 0; i < this.systems.length; ++i) {
            if(this.systems[i] == sys) {
                this.systems.splice(i,1);
                return;
            }
        }
    }

    shutdown() {
        clearTimeout(this.timeoutHandle);
        this.server.stop();
    }

    onExit() {
    }
}

module.exports.Automate = AutomateController;


var moment = require('moment');
var abstraction = require('./abstraction');
var tools = require('./tools');

class TimeRangeCondition extends abstraction.Condition
{
    constructor(config) {
        super(config);
        this.setup(config);
    }

    setup(config) {
        this.onChange = config.onChange;
        this.end = null;
        this.state = false;

        this.time = {
            hour: config.start.hour != undefined ? config.start.hour : 0,
            minute: config.start.minute != undefined ? config.start.minute : 0,
            second: config.start.second != undefined ? config.start.second : 0
        };

        this.duration = moment.duration({
            seconds: config.duration.second != undefined ? config.duration.second : 0,
            minutes: config.duration.minute != undefined ? config.duration.minute : 0,
            hours: config.duration.hour != undefined ? config.duration.hour : 0,
            days: config.duration.day != undefined ? config.duration.day : 0,
            weeks: config.duration.week != undefined ? config.duration.week : 0,
            months: config.duration.month != undefined ? config.duration.month : 0,
            years: config.duration.year != undefined ? config.duration.year : 0
        });
    }

    update() {
        if(!this.isAlive()) {
            return;
        }
        var now = moment();
        if(!this.state) {
            // We are waiting for the state to begin
            var start = moment().hour(this.time.hour).minute(this.time.minute).second(this.time.second);
            var end = moment(start).add(this.duration);

            if(now.isAfter(end))
            {
                // this is a temporary hack.. I have to invent how to time lamps to go off and on.. properly
            }
            else if(now.isAfter(start)) {
                this.state = true;
                this.end = end;
                this.onChange(true);
            }
        }
        else {
            if(now.isAfter(this.end)) {
                this.state = false;
                this.end = null;
                this.onChange(false);
            }
        }
    }

    toString()
    {
        var start = moment().hour(this.time.hour).minute(this.time.minute).second(this.time.second);
        var end = moment().hour(this.time.hour).minute(this.time.minute).second(this.time.second).add(this.duration);

        return start.format() + " -> " + end.format();
    }
}

module.exports.TimeRange = TimeRangeCondition;
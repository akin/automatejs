
var http = require('http');
var url = require('url');
var path = require('path');

var abstraction = require('./abstraction');
var tools = require('./tools');
var httptools = require('./httptools');
var fs = require('fs');

class Server
{
    constructor(config) {
        this.server = null;
        this.port = 80;
        this.httptools = new httptools.HTTPTools();
        this.parent = null;
        this.setup(config);
    }

    setup(config) {
        if(config == null) {
            return;
        }

        var server = this;
        this.parent = config.parent;
        this.server = http.createServer(function (req, res) {
            server.handle(req,res);
        });
        this.port = config.port;
        this.server.listen(this.port);

        tools.Log("Server listening to " + config.port + " happy to serve.");        
    }

    stop() {
        if(this.server != null) {
            this.server.close();
            this.server = null;
            tools.Log("Server closing " + this.port + " happy to serve."); 
        }
    }

    handle(request, response) {
        var result = this.httptools.evaluate(request);

        var handled = false;
        if(result.method == 'get') {
            if(result.dynamic) {
                var serializer = this.httptools.serializeType(result.type);
                response.writeHead( 200, { 
                    'Content-Type': this.httptools.getContentType(result.type) 
                });
                response.end(
                    serializer({
                        name: result.name,
                        data: this.parent.get({ 
                            type: result.name
                        })
                    }), 
                    'utf-8'
                );
                handled = true;
            }
            else {
                this.httptools.handleFile(result, response);
                handled = true;
            }
        }
        else if(result.method == 'post') {
            handled = this.handlePost(request, result, response);
        }

        if(!handled) {
            response.writeHead(500, {'Content-Type': 'text/html'});
            response.end("Server error, unsupported request (method).");
        }
    }

    onAction(config) {
        if(config.action == "shutdown") {
            this.parent.shutdown();
            return;
        }
        if(config.handle != null) {
            // need to find handle object to deal this..
            var handler = this.parent.getSystemHandler(config.handle);
            if(handler != null) {
                handler.onAction(config);
            }
            else {
                tools.Log("Server closing could not find handler for " + config.handle);    
            }
            return;
        }
    }

    handlePost(request, result, response) {
        var that = this;
        var data = "";
        request.on("data", function(data_) {
            data += data_;
        }).on("end", function() {
            tools.Log("Post data: " + data); 
            if (data) {
                var object = JSON.parse(data);

                that.onAction(object);
            } else {
                // FOO
            }

            response.writeHead(200, {'Content-Type': 'text/html'});
            response.end('post received');
        });

        // Send request to:
        /// named system, disable condition
        /// named system, set system state to on/off
        /// shutdown nodejs server
        /// add new systems
        /// save configs
        /// rename a system to your choosing
        /// fully datadriven systems that can be serialized to json
        // content is json?
        return true;
    }
}

module.exports.Server = Server;
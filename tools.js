
'use strict';
var moment = require('moment');
const exec = require('child_process').exec;

function Log(msg)
{
    console.log(moment().format() + " | " + msg);
}

function Command(config)
{
    var command = config.command;
    if(config.args != null) {
        command += " " + config.args.join(" ");
    }

    exec(command, function(error, stdout, stderr) {
        if (error != null) {
            if(config.onError != null) {
                config.onError("shell",error);
            }
            else {
                Log('exec error: ' + error);
            }
        }

        if(stdout.length > 0) {
            if(config.onMessage != null) {
                config.onMessage("stdout",stdout);
            }
        }
    });
}

module.exports.Log = Log;
module.exports.Command = Command;
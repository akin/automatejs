

var fs = require('fs');
var path = require('path');
var mustache = require('mustache');
var tools = require('./tools');

var dynamicPath = "/data/";

class HTTPTools
{
    constructor(config) {
    }
    getContentType(value)
    {
        switch(value)
        {
            case 'html': return 'text/html';
            case 'js': return 'text/javascript';
            case 'css': return 'text/css';
            case 'json': return 'application/json';
            case 'png': return 'image/png';
            case 'jpg': return 'image/jpg';
            case 'wav': return 'audio/wav';
            default: break;
        }
        return 'text/unknown';
    }

    serializeType(value) {
        switch(value) {
            case "json" : {
                return function(data) {
                    return JSON.stringify(data);
                }
            }
        }
        return function(data) { return data; };
    }

    evaluate(request) {
        var result = {
            method: request.method.toLowerCase(), // get,delete,post
            path: "",
            name: "",
            type: "",
            dynamic: false,
        };

        result.path = request.url;
        if (result.path == '/') {
            result.path = "index.html";
        }
        var sub = path.extname(result.path).toLowerCase();
        var len = result.path.length;
        result.name = result.path.substring(0, len - sub.length);
        switch (sub) {
            case '.htm':
            case '.txt':
            case '.html':
                result.type = "html";
                break;
            case '.js':
                result.type = "js";
                break;
            case '.css':
                result.type = "css";
                break;
            case '.json':
                result.type = "json";
                break;
            case '.png':
                result.type = "png";
                break;      
            case '.jpg':
                result.type = "jpg";
                break;
            case '.wav':
                result.type = "wav";
                break;
            default:
        }
        if(result.path.startsWith(dynamicPath)) {
            result.name = result.name.substring(dynamicPath.length);
            result.path = result.path.substring(dynamicPath.length);
            result.dynamic = true;
        }
        return result;
    }

    handleFile(result, response) {
        var contentType = this.getContentType(result.type);

        fs.readFile("client/" + result.path, function(error, content) {
            if (error) {
                if(error.code == 'ENOENT'){
                    response.writeHead(404);
                    response.end('Sorry, 404: ' + error.code + ' ..\n');
                    response.end(); 
                }
                else {
                    response.writeHead(500);
                    response.end('Sorry, check with the site admin for error: ' + error.code + ' ..\n');
                    response.end(); 
                }
            }
            else {
                response.writeHead(200, { 'Content-Type': contentType });
                response.end(content, 'utf-8');
            }
        });
    }

    page(request, response, template, view) { 
        response.write(mustache.to_html(template, view), 'utf-8');
    }
}

module.exports.HTTPTools = HTTPTools;
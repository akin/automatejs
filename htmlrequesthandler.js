
var abstraction = require('./abstraction');
var tools = require('./tools');
var condition = require('./condition');

class HTMLRequestHandler extends abstraction.HTTPRequestHandler
{
    constructor(config) {
        super(config);
    }

    handle(request) {
        return true;
    }

    toObject() {
        var system = this.system;

        return {
            name: system.getHandle(),
            state: system.getState(),
            condition: system.getCondition().toString()
        };
    }

    onAction(config) {
        var system = this.system;

        // "action":"condition","state":false,"handle":"w9n78ro74y8gtxkwqaor"
        switch(config.action) {
            case "condition": 
                system.getCondition().setAlive(config.state);
                break;
            case "state": 
                system.setState(config.state);
                break;
            default:
                break;
        }
    }
}

module.exports.HTMLRequestHandler = HTMLRequestHandler;

var abstraction = require('./abstraction');
var tools = require('./tools');
var condition = require('./condition');

class PlantLightSystem extends abstraction.System
{
    constructor(config) {
        super(config);
        this.platform = config.platform;
        this.handle = null;

        if(!this.platform.isSupported()) {
            this.platform.install({
                onError: function(msg) {
                },
                onSuccess: function() {
                }
            });
        }

        var that = this;
        this.condition = new condition.TimeRange({
            start: config.start,
            duration: config.duration,
            onChange: function(state) {
                that.setState(state);
            }
        });

        this.pin = config.pin;
        this.state = false;

        tools.Log("Setting things on and off, this purges the real world state.");
        this.setState(true);
        this.setState(false);

        tools.Log("Setup complete.");
        tools.Log("Starting PlantLightSystem, the lights will be ON " + this.condition.toString());
    }

    setState(state) {
        this.state = state;
        var self = this;
        tools.Log("setState " + (state ? "on" : "off"));

        var forceState = function() {
            self.platform.read({
                pin: self.pin,
                state: state,
                onError: function(msg) {
                    tools.Log("ERROR " + msg);
                },
                onSuccess: function(msg) {
                    if(self.state != msg) {
                        tools.Log("Warning, inconsistant plantlight state: " + msg + " should be " + self.state + ".");
                        self.setState(self.state);
                    }
                }
            });
        };

        this.platform.write({
            pin: this.pin,
            state: state,
            onError: function(msg) {
                Log("Failed to write the state " + msg);
                forceState();
            },
            onSuccess: function() {
                forceState();
            }
        });
    }

    getState() {
        return this.state;
    }
    
    getCondition() {
        return this.condition;
    }

    update() {
        this.condition.update();
    }

    onExit() {
    }
}

module.exports.PlantLight = PlantLightSystem;
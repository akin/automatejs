
var automate = require('./automate');
var plantlight = require('./plantlight');
var gpio = require('./gpio');
var requesthandler = require('./htmlrequesthandler');

var controller = new automate.Automate({
    server: {
        port: 8080
    }
});
// plantlight#1
var system = new plantlight.PlantLight({
    start: {
        hour: 8,
        minute: 0,
        second: 0
    },
    duration: {
        hour: 15
    },
    pin: 23,
    platform: new gpio.Gpio()
});
controller.addSystem({
    system: system,
    requestHandler: new requesthandler.HTMLRequestHandler({system: system})
});
// plantlight#2
system = new plantlight.PlantLight({
    start: {
        hour: 8,
        minute: 0,
        second: 0
    },
    duration: {
        hour: 15
    },
    pin: 24,
    platform: new gpio.Gpio()
});
controller.addSystem({
    system: system,
    requestHandler: new requesthandler.HTMLRequestHandler({system: system})
});


var abstraction = require('./abstraction');
var tools = require('./tools');

class GpioPlatform extends abstraction.Platform
{
    write(config) {
        // if gpio does not work, sudo apt-get install wiringPi
        var success = true;
        tools.Command({
            command: "gpio",
            args: ["-g","mode", config.pin.toString(), "out"],
            onError: function(sys, msg) {
                config.onError("Failed to select mode. " + msg);
                success = false;
            },
            onMessage: function(sys,msg) {
            }
        });
        if(success) {
            tools.Command({
                command: "gpio",
                args: ["-g","write", config.pin.toString(), config.state ? "1" : "0"],
                onError: function(sys, msg) {
                    config.onError("Failed to write. " + msg);
                    success = false;
                },
                onMessage: function(sys,msg) {
                }
            });
        }
        if(success) {
            config.onSuccess();
        }
    }

    read(config) {
        tools.Command({
            command: "gpio",
            args: ["-g","read", config.pin.toString()],
            onError: function(sys, msg) {
                config.onError("Failed to read. " + msg);
            },
            onMessage: function(sys,msg) {
                // !! remove Linebreaks..
                var message = msg.replace("\n", "");
                config.onSuccess(message == "1");
            }
        });
    }

    isSupported() {
        return true;
    }

    install(config) {
        var success = true;
        tools.Command({
            command: "sudo",
            args: ["apt-get","install", "wiringPi"],
            onError: function(sys, msg) {
                config.onError("Failed to install wiringPi. " + msg);
                success = false;
            },
            onMessage: function(sys,msg) {
            }
        });
        if(success) {
            config.onSuccess();
        }
    }
}

module.exports.Gpio = GpioPlatform;
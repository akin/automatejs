
var systemTpl = "/tpl/system.html";

class Client
{
    constructor(config) {
        this.template = {};

        var that = this;
        this.loadTemplate({
            item: [
                systemTpl
            ],
            onComplete: function(item){
                that.setup(config);
            }
        })
    }

    loadTemplate(config) {
        if(config.item == null) {
            config.onComplete(config.item);
            return;
        }
        if(Array.isArray(config.item)) {
            var that = this;
            var count = config.item.length;
            var arrayCompletion = function(cItem) {
                --count;
                if(count == 0) {
                    config.onComplete(config.item);
                }
            };
            config.item.forEach(function(item) {
                that.loadTemplate({
                    item: item,
                    onComplete: arrayCompletion
                });
            }, this);
            return;
        }

        if(this.template[config.item] != null) {
            config.onComplete();
            return;
        }
        var that = this;
        $.get(config.item, function(data) {
            that.template[config.item] = data;
            config.onComplete(config.item);
        });
    }

    setup(config) {
        this.connect();
    }

    getTemplate(key) {
        return this.template[key];
    }

    connect() {
        var that = this;
        $.getJSON( "/data/all.json", function(data) {
            // {name:"all", data:[]}
            if(data != null && data.data != null) {
                that.setSystem(data.data);
            }
        });
    }

    setSystem(data) {
        $("#content").html("");

        var that = this;
        $.each(data, function( index, value ) {
            value["stateUnicode"] = value.state ? "&#128161;" : "&#128164;";
            $("#content").append(
                Mustache.render(that.getTemplate(systemTpl), value)
            );

            that.refresh();
        });
    }

    refresh() {
        var that = this;
        $('[data-action]').each(function(index){
            var button = $(this);
            $(this).off( "click");
            $(this).on( "click", function() {
                that.on({
                    action: "click",
                    item: button
                });
            });
        });
    }

    on(config) {
        if(config.action == "click") {
            $.post("", JSON.stringify(config.item.data("action")), function(result){
                $("span").html(result);
                alert(result);
            });
        }
    }
}

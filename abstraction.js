
class PlatformInterface
{
    write(config) {
    }

    read(config) {
    }

    isSupported() {
    }

    install() {
    }
}

class SystemInterface
{
    constructor(config) {
    }

    register(config) {
        this.handle = config.handle;
    }

    getHandle() {
        return this.handle;
    }
    
    setState(state) {
    }

    getState() {
        return false;
    }

    getCondition() {
        return null;
    }

    update() {
    }

    onExit() {
    }
}

class HTTPRequestHandler
{
    constructor(config) {
        this.system = config.system;
    }
    
    handle(request, response) {
    }
}

class ConditionInterface
{
    constructor(config)
    {
        this.alive = true;
        if(config.alive != null) {
            this.alive = config.alive;
        }
    }

    update() 
    {
    }

    setAlive(state)
    {
        this.alive = state;
    }

    isAlive()
    {
        return this.alive;
    }

    onChange(config)
    {
    }

    toString()
    {
        return "";
    }
}

module.exports.System = SystemInterface;
module.exports.Platform = PlatformInterface;
module.exports.Condition = ConditionInterface;
module.exports.HTTPRequestHandler = HTTPRequestHandler;